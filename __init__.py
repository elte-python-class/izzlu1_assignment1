class Protein:
	def __init__(self, string):
		self.protein = string.split("\n")[1:]
		self.metadata = string.split("\n")[0]
		data = self.metadata.split(" ")
		self.id = data[0].split("|")[1]
		self.name = data[0].split("|")[2]
		del data[0]
		for d in data:
			if not d.startswith("OS"):
				self.name += " " + d
			else:
				os_start = data.index(d)
				break

		for d in data:
			if(d.startswith("OX")):
				self.ox = int(d.split("=")[1])
				os_end = data.index(d)
			if(d.startswith("GN")):
				self.gn = d.split("=")[1]
			if(d.startswith("PE")):
				self.pe = int(d.split("=")[1])
			if(d.startswith("SV")):
				self.sv = int(d.split("=")[1])

		os_string = data[os_start:os_end]
		self.os = os_string[0].split("=")[1]
		for idx, val in enumerate(os_string):
			if idx != 0:
				self.os += " " + os_string[idx]

		self.attach = string.replace(" ", "")
		getn = self.attach.split("\n")
		getrid = getn[1:]
		aminoacids = "".join(getrid)[:]
		self.aminoacids = "".join(getrid)[:]
		self.size = len(aminoacids)

		straa = str(aminoacids)
		dictcounter ={}
		for d in straa:
			if not dictcounter or d not in dictcounter:
				dictcounter.update({d : 1})
			elif d in dictcounter:
				dictcounter[d] += 1
		self.aminoacidcounts = dictcounter

	def __repr__(self):
		header = f"{self.name} id: {self.id}"
		return header

	def __eq__(self, other):
		return((self.size) == (other.size))

	def __ne__(self,other):
		return((self.size) != (other.size))

	def __lt__(self,other):
		return((self.size) < (other.size))

	def __le__(self,other):
		return((self.size) <= (other.size))

	def __gt__(self,other):
		return((self.size) > (other.size))

	def __ge__(self,other):
		return((self.size) >= (other.size))


def sort_proteins_pe(proteins):
	return sorted(proteins, key = lambda p: p.pe, reverse = True)
	


def sort_proteins_aa(proteins, aminoacids):
	return sorted(proteins, key = lambda p: p.aminoacidcounts[aminoacids], reverse = True) 
	

def find_protein_with_motif(proteins, motif):
	protmotif = []
	for d in proteins:
		if motif in d.aminoacids:
			protmotif.append(d)
	return protmotif
	
def load_fasta(fasta):
	open_fasta = open(fasta, "r").read()
	split_fasta = open_fasta.split(">sp")
	prot_good = []
	for p in split_fasta[1:]:
		prot_good.append(Protein(p))

	return  prot_good
